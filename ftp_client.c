#include <stdio.h>
#include <memory.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#define FTP_CONTROL_PORT 21
#define BUF_SIZE 1024
#define PASV_MESSAGE_SIZE 6

#define DEFAULT_USERNAME "anonymous"
#define DEFAULT_PASSWORD "anonymous@domain.com"

#define FILE_OK 150
#define AUTH_OK 230

char message_buffer[BUF_SIZE];

int connect_to_server(char *srv_hostname, char *ip, int port) {
    int socket_descriptor;
    struct sockaddr_in server;
    struct in_addr **addr_list;

    socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_descriptor == -1) {
        return -1;
    }

    if (srv_hostname != NULL) {
        struct hostent *hp;
        hp = gethostbyname(srv_hostname);
        if (hp == NULL) return -1;
        addr_list = (struct in_addr **) hp->h_addr_list;
        server.sin_addr.s_addr = addr_list[0]->s_addr;
    } else if (ip != NULL) {
        server.sin_addr.s_addr = inet_addr(ip);
    }
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    if (connect(socket_descriptor, (struct sockaddr *) &server, sizeof(server)) < 0) {
        return -1;
    }

    return socket_descriptor;
}

void read_response(int socket_descriptor) {
    memset(message_buffer, '\0', BUF_SIZE);
    recv(socket_descriptor, message_buffer, BUF_SIZE, 0);
}

void print_response(int socket_descriptor) {
    read_response(socket_descriptor);
    printf("ftp>\t%s", message_buffer);
}

int check(char *msg, int pattern) {
    int code;
    sscanf(message_buffer, "%d", &code);
    if (code != pattern) {
        return -1;
    }
    return 0;
}

void send_command(const char *command, const char *argument, int socket_descriptor) {
    char command_buf[BUF_SIZE];
    memset(command_buf, '\0', BUF_SIZE);
    if (argument != NULL) {
        sprintf(command_buf, "%s %s\r\n", command, argument);
    } else {
        sprintf(command_buf, "%s\r\n", command);
    }
    fflush(stdout);
    printf("  $>\t%s", command_buf);
    send(socket_descriptor, command_buf, strlen(command_buf), 0);
}

int auth(const char *login, const char *pass, int socket_descriptor) {
    send_command("USER", login, socket_descriptor);
    print_response(socket_descriptor);
    send_command("PASS", pass, socket_descriptor);
    print_response(socket_descriptor);
    return check(message_buffer, AUTH_OK);
}

int *parse_pasv_msg(char *msg) {
    char *tmp;
    strtok(msg, "(");
    tmp = strtok(NULL, ")");
    int *result = (int *) malloc(sizeof(int) * PASV_MESSAGE_SIZE);
    sscanf(tmp, "%d,%d,%d,%d,%d,%d", &result[0], &result[1], &result[2], &result[3],
           &result[4], &result[5]);
    return result;
}

int set_passive(int socket_descriptor) {
    send_command("PASV", NULL, socket_descriptor);
    print_response(socket_descriptor);
    int *data = parse_pasv_msg(message_buffer);
    int port = data[PASV_MESSAGE_SIZE - 2] * 256 + data[PASV_MESSAGE_SIZE - 1];
    char ip[24];
    sprintf(ip, "%d.%d.%d.%d", data[0], data[1], data[2], data[3]);
    free(data);
    return connect_to_server(NULL, ip, port);
}

const char *parse_filename(const char *path) {
    const char *filename;
    if ((filename = strrchr(path, '/')) == NULL) {
        filename = path;
    } else {
        filename++;
    }
    return filename;
}

int get_file(const char *path, int socket_descriptor, int data_socket_descriptor) {
    send_command("RETR", path, socket_descriptor);
    print_response(socket_descriptor);
    if (check(message_buffer, FILE_OK) != 0) {
        return -1;
    }
    const char *filename = parse_filename(path);

    int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0664);
    if (fd == -1) {
        return -1;
    }
    char download_buffer[BUF_SIZE];
    long count = 0;
    long current_count;

    while ((current_count = recv(data_socket_descriptor, download_buffer, BUF_SIZE, 0)) > 0) {
        write(fd, download_buffer, current_count);
        count += current_count;
    }
    printf("ftp>\tDownloaded %ld bytes\n", count);
    print_response(socket_descriptor);
    close(fd);
    return 0;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        puts("No argument");
        return -1;
    }
    int socket_descriptor;
    if ((socket_descriptor = connect_to_server(argv[1], NULL, FTP_CONTROL_PORT)) < 0) {
        puts("Connection error");
        return -1;
    }
    print_response(socket_descriptor);
    int custom_log_and_pass = argc > 4;
    int auth_result;
    if (custom_log_and_pass) {
        auth_result = auth(argv[3], argv[4], socket_descriptor);
    } else {
        auth_result = auth(DEFAULT_USERNAME, DEFAULT_PASSWORD, socket_descriptor);
    }
    if (auth_result < 0) {
        close(socket_descriptor);
        return -1;
    }
    send_command("TYPE", "I", socket_descriptor);
    print_response(socket_descriptor);
    int data_socket_descriptor = set_passive(socket_descriptor);
    if (data_socket_descriptor < 0) {
        puts("Connection error");
        close(socket_descriptor);
        return -1;
    }
    if (get_file(argv[2], socket_descriptor, data_socket_descriptor) < 0) {
        puts("File download error");
        close(socket_descriptor);
        close(data_socket_descriptor);
        return -1;
    }
    send_command("QUIT", NULL, socket_descriptor);
    print_response(socket_descriptor);
    close(socket_descriptor);
    close(data_socket_descriptor);
    return 0;
}